# Copyright (C) 2006--2014  Kipp Cannon
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


from tqdm import tqdm
from .. import __author__, __date__, __version__
from .. import lsctables


#
# =============================================================================
#
#                            Time Slide Comparison
#
# =============================================================================
#


def time_slides_vacuum(time_slides, verbose = False):
	"""
	Given a dictionary mapping time slide IDs to instrument-->offset
	mappings, for example as returned by the as_dict() method of the
	TimeSlideTable class in ligo.lw.lsctables, construct and return a
	mapping indicating time slide equivalences.  This can be used to
	delete redundant time slides from a time slide table, and then also
	used via the applyKeyMapping() method of ligo.lw.table.Table
	instances to update cross references (for example in the
	coinc_event table).

	Example:

	>>> slides = {0: {"H1": 0, "H2": 0}, 1: {"H1": 10, "H2": 10}, 2: {"H1": 0, "H2": 10}}
	>>> time_slides_vacuum(slides)
	{1: 0}

	indicating that time slide ID 1 describes a time slide that is
	equivalent to time slide ID 0.  The calling code could use this
	information to delete time slide ID 1 from the time_slide table,
	and replace references to that ID in other tables with references
	to time slide ID 0.
	"""
	# convert offsets to deltas
	time_slides = dict((time_slide_id, offsetvect.deltas) for time_slide_id, offsetvect in time_slides.items())
	with tqdm(total=len(time_slides), disable=not verbose) as progressbar:
		# old --> new mapping
		mapping = {}
		# while there are time slide offset dictionaries remaining
		while time_slides:
			# pick an ID/offset dictionary pair at random
			id1, deltas1 = time_slides.popitem()
			# for every other ID/offset dictionary pair in the time
			# slides
			ids_to_delete = []
			for id2, deltas2 in time_slides.items():
				# if the relative offset dictionaries are
				# equivalent record in the old --> new mapping
				if deltas2 == deltas1:
					mapping[id2] = id1
					ids_to_delete.append(id2)
			for id2 in ids_to_delete:
				time_slides.pop(id2)
			# number of offset vectors removed from time_slides
			# in this iteration
			progressbar.update(1 + len(ids_to_delete))
	# done
	return mapping
